package be.kdg.java3.relationsdemo;

import org.springframework.boot.CommandLineRunner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;

//@Component
public class JdbcTests implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        try (Connection connection =
               DriverManager.getConnection("jdbc:h2:mem:studentsdb", "sa", "");
             Statement statement = connection.createStatement()
        ) {
            //Query the database:
            try (ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS")) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("ID");
                    String name = resultSet.getString("NAME");
                    double length = resultSet.getDouble("LENGTH");
                    LocalDate date = resultSet.getDate("BIRTHDAY").toLocalDate();
                    System.out.println(id + " " + name + " " + length + " " + date);
                }
            }

            //Do an insert:
            int insertResult = statement.executeUpdate("INSERT INTO STUDENTS(NAME, LENGTH, BIRTHDAY) " +
              "VALUES ( 'Jan', 1.89, '1987-04-25')");
            System.out.println("Number of rows inserted:" + insertResult);

            //Do a delete:
            int deleteResuit = statement.executeUpdate("DELETE FROM STUDENTS WHERE NAME='Jan'");
            System.out.println("Number of rows deleted:" + deleteResuit);

        }
    }
}
