package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;

public class Student {
    private int id;
    private String name;
    private double length;
    private LocalDate birthday;

    public Student(int id, String name, double length, LocalDate birthday) {
        this( name,length,birthday);
        this.id=id;
    }

    public Student(String name, double length, LocalDate birthday) {
        this.name = name;
        this.length = length;
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lenght=" + length +
                ", birthday=" + birthday +
                '}' ;
    }
}
