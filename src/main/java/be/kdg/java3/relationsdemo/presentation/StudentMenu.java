package be.kdg.java3.relationsdemo.presentation;

import be.kdg.java3.relationsdemo.domain.Student;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

@Component
public class StudentMenu implements CommandLineRunner, ApplicationContextAware {
	private final Scanner scanner = new Scanner(System.in);
	private final StudentRepository studentRepository;
		private ApplicationContext ctx;


		private final List<MenuItem> menu = List.of(
		new MenuItem("list all students", this::listAllStudents),
		new MenuItem("add a student", this::addStudent),
		new MenuItem("update a student", this::updateStudent),
		new MenuItem("delete a student", this::deleteStudent),
		new MenuItem("change address of student", this::changeAddressOfStudent),
		new MenuItem("list all schools", this::listAllSchools),
		new MenuItem("add a school", this::addSchool),
		new MenuItem("delete a school", this::deleteSchool),
		new MenuItem("change school of student", this::changeSchoolOfStudent),
		new MenuItem("list all students of school", this::showAllStudentsOfSchool),
		new MenuItem("add course", this::addCourse),
		new MenuItem("delete course", this::deleteCourse),
		new MenuItem("add student to course", this::addStudentToCourse),
		new MenuItem("list students of course", this::listStudentsOfCourse),
		new MenuItem("list courses of student", this::listCoursesOfStudent)
	);



	public StudentMenu(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public void run(String... args) {
		show();
	}

	public void show() {
			boolean active = true;
		while (active) {
			System.out.print("""
				Welcome to the Student Management System
				========================================
				0) Exit
				""");
			for (int i = 0; i < menu.size(); i++) {
				System.out.println((i + 1) + ") " + menu.get(i).text());
			}
			System.out.print("Your choice: ");
			int choice = Integer.parseInt(scanner.nextLine());
			if (choice < 0 || choice > menu.size()) {
				System.out.println("Invalid choice");
			} else if (choice == 0) {
				System.out.println("Exiting...");
				active=false;
				((ConfigurableApplicationContext) ctx).close();
			} else {
				menu.get(choice - 1).action().run();
			}
		}
	}

		private void addCourse() {
			todo();
		}




	private void deleteCourse() {
		todo();
	}

	private void listCoursesOfStudent() {
		todo();
	}

	private void listStudentsOfCourse() {
		todo();
	}

	private void addStudentToCourse() {
		todo();
	}


	private void addSchool() {
		todo();

	}

	private void listAllSchools() {
		todo();

	}

	private void deleteSchool() {
		todo();
	}

	private void showAllStudentsOfSchool() {
		todo();
	}

	private void changeSchoolOfStudent() {
		todo();
	}

	private void changeAddressOfStudent() {
		todo();
	}

	private void listAllStudents() {
		try {
			studentRepository.findAll().forEach(System.out::println);
		} catch (RuntimeException dbe) {
			System.out.println("Unable to find all students:");
			System.out.println(dbe.getMessage());
		}
	}

	private void changeSchoolOfStudent(Student student) {
		todo();
	}

	private void addStudent() {
		Student student = askStudentAttributes(false);
		try {
			Student createdStudent
				= studentRepository.createStudent(student);
			System.out.println("Student added to database:" + createdStudent);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
		}
	}



	private void updateStudent() {
		Student student = askStudentAttributes(true);
		try {
			studentRepository.updateStudent(student);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
		}
	}

	private void deleteStudent() {
		int id = chooseStudentId();
		try {
			studentRepository.deleteStudent(id);
		} catch (RuntimeException dbe) {
			System.out.println("Problem:" + dbe.getMessage());
		}
	}

		private Student askStudentAttributes(boolean askId) {
		int id = 0;
		if (askId) {
			id = chooseStudentId();
		}
		System.out.print("Name: ");
		String name = scanner.nextLine();
		System.out.print("Length: ");
		double length = Double.parseDouble(scanner.nextLine());
		System.out.print("Birthday (dd-mm-yyyy): ");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
		return new Student(id,name, length, birthday);
	}

		private int chooseStudentId() {
		listAllStudents();
		System.out.print("Id: ");
		return Integer.parseInt(scanner.nextLine());
	}

		private  void todo() {
		System.out.println(" \t-> We're working on that!");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
				ctx = applicationContext;

	}
}
