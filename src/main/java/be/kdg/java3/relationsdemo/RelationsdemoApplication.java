package be.kdg.java3.relationsdemo;

import be.kdg.java3.relationsdemo.presentation.StudentMenu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class RelationsdemoApplication {

    public static void main(String[] args) {
       SpringApplication.run(RelationsdemoApplication.class, args);
    }

}
