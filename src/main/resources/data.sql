INSERT INTO STUDENTS (ID, NAME, LENGTH, BIRTHDAY)
VALUES (1, 'Jan', 1.89, '1987-04-25'),
       (2, 'Jef', 1.81, '1977-04-25'),
       (3, 'Mie', 1.67, '1967-04-25'),
       (4, 'Fien', 1.34, '1937-04-25'),
       (5, 'Erica', 1.56, '1987-05-25'),
       (6, 'Bart', 2, '1987-04-26'),
       (7, 'Josefien', 1.89, '1983-04-25'),
       (8, 'Elke', 1.8, '1981-04-15'),
       (9, 'Erna', 1.99, '1982-01-25');
ALTER TABLE STUDENTS
    ALTER COLUMN ID RESTART WITH (SELECT MAX(ID) FROM STUDENTS) + 1;